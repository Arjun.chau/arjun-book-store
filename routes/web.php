<?php

use App\Http\Controllers\Backend\AuthController;
use App\Http\Controllers\Backend\AuthorController;
use App\Http\Controllers\Backend\BookController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Frontend\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('admin/login', [AuthController::class, 'index'])->name('admin.login');
Route::post('login', [AuthController::class, 'login'])->name('login');

// frontend
Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('book-detail/{slug}', [HomeController::class, 'bookDetailBySlug'])->name('book-detail');

Route::group(['middleware' => ['auth']], function () {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

//    Dashboard
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

//    Category
    Route::group(['prefix' => 'category'], function (){
        Route::get('index', [CategoryController::class, 'index'])->name('category.index');
        Route::get('create', [CategoryController::class, 'create'])->name('category.create');
        Route::post('store', [CategoryController::class, 'store'])->name('category.store');
        Route::get('edit/{category}', [CategoryController::class, 'edit'])->name('category.edit');
        Route::get('show/{category}', [CategoryController::class, 'show'])->name('category.show');
        Route::post('update/{category}', [CategoryController::class, 'update'])->name('category.update');
        Route::get('delete/{category}', [CategoryController::class, 'delete'])->name('category.delete');
    });

    //    Author
    Route::group(['prefix' => 'author'], function (){
        Route::get('index', [AuthorController::class, 'index'])->name('author.index');
        Route::get('create', [AuthorController::class, 'create'])->name('author.create');
        Route::post('store', [AuthorController::class, 'store'])->name('author.store');
        Route::get('edit/{author}', [AuthorController::class, 'edit'])->name('author.edit');
        Route::get('show/{author}', [AuthorController::class, 'show'])->name('author.show');
        Route::post('update/{author}', [AuthorController::class, 'update'])->name('author.update');
        Route::get('delete/{author}', [AuthorController::class, 'delete'])->name('author.delete');
    });

    //    Book
    Route::group(['prefix' => 'book'], function (){
        Route::get('index', [BookController::class, 'index'])->name('book.index');
        Route::get('create', [BookController::class, 'create'])->name('book.create');
        Route::post('store', [BookController::class, 'store'])->name('book.store');
        Route::get('edit/{book}', [BookController::class, 'edit'])->name('book.edit');
        Route::post('update/{book}', [BookController::class, 'update'])->name('book.update');
        Route::get('delete/{book}', [BookController::class, 'delete'])->name('book.delete');
    });

});

