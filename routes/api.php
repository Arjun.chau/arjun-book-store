<?php

use App\Http\Controllers\Api\ApiBookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'book'], function (){
    Route::get('index', [ApiBookController::class, 'index'])->name('book.index');
    Route::post('store', [ApiBookController::class, 'store'])->name('book.store');
    Route::put('update/{book}', [ApiBookController::class, 'update'])->name('book.update');
    Route::delete('delete/{book}', [ApiBookController::class, 'delete'])->name('book.delete');
    Route::get('detail/{book}', [ApiBookController::class, 'bookDetail'])->name('book.detail');
    Route::get('detail-by-category', [ApiBookController::class, 'bookDetailByCategory'])->name('book.detail-by-category');
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
