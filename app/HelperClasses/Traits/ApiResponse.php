<?php

namespace App\HelperClasses\Traits;

trait ApiResponse
{

    public function sendApiSuccessResponse($data = [], $message = 'Success Response', $status = 200, $headers = []) {
        return response()->json([
            'data' => $data,
            'message' => $message,
            'status' => 'success',
        ], $status, $headers);
    }

    public function sendApiErrorResponse($message = 'Error Response' ,$status= 500, $headers = []) {
        return response()->json([
            'message' => $message,
            'status' => 'error',
        ], $status, $headers);
    }

}
