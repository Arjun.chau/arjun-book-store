<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'book_no' => 'nullable',
            'publication_date' => 'nullable',
            'image' => 'required|mimes:jpg,png,jpeg',
//            'is_active' => 'required',
            'category_id' => 'required',
            'author_id' => 'required',
        ];
    }
}
