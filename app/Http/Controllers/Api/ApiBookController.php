<?php

namespace App\Http\Controllers\Api;

use App\HelperClasses\Traits\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Models\Book;
use App\Repositories\Interfaces\BookInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiBookController extends Controller
{
    use ApiResponse;
    private $bookRepository;

    public function __construct(BookInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }
    /**
     * @OA\Get(
     * path="/api/book/index",
     * operationId="bookIndex",
     * tags={"Book"},
     * summary="Book Index",
     * description="Book Index",
     *      @OA\Response(
     *          response=200,
     *          description="Books List successfully.",
     *          @OA\JsonContent(
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="description", type="string"),
     *                  @OA\Property(property="book_no", type="string"),
     *                  @OA\Property(property="publication_date", type="string"),
     *                  @OA\Property(property="image", type="string"),
     *                  @OA\Property(property="is_active", type="integer"),
     *                  @OA\Property(property="category_id", type="integer"),
     *                  @OA\Property(property="author_id", type="integer"),
     *                  @OA\Property(property="added_by", type="integer"),
     *                  @OA\Property(property="updated_by", type="integer"),
     *                  @OA\Property(property="category", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="author", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                          @OA\Property(property="bio", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="added_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="updated_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *              ),
     *          )
     *       ),
     *     @OA\Header(
     *        header="Authorization",
     *        description="Authorization header with Bearer token",
     *        required=true,
     *        @OA\Schema(
     *            type="string",
     *        )
     *    )
     * )
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $books = $this->bookRepository->index();
        return $this->sendApiSuccessResponse($books, 'Book Lists successfully.');
    }

    /**
     * @OA\Post(
     * path="/api/book/store",
     * operationId="bookStore",
     * tags={"Book"},
     * summary="Book Store",
     * description="Book Store",
     *     @OA\RequestBody(
     *          required=true,
     *          description="Book data to Store",
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *               type="object",
     *               required={"title", "description", "image", "category_id","author_id"},
     *               @OA\Property(property="title", type="string"),
     *               @OA\Property(property="description", type="string"),
     *               @OA\Property(property="book_no", type="string", nullable=true),
     *               @OA\Property(property="publication_date", type="string",format="date", nullable=true),
     *               @OA\Property(property="image", type="string" ,format="binary"),
     *               @OA\Property(property="is_active", type="boolean"),
     *               @OA\Property(property="category_id", type="integer"),
     *               @OA\Property(property="author_id", type="integer"),
     *            ),
     *        ),
     *    ),
     *      @OA\Response(
     *          response=200,
     *          description="Books Stored successfully.",
     *          @OA\JsonContent(
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="description", type="string"),
     *                  @OA\Property(property="book_no", type="string"),
     *                  @OA\Property(property="publication_date", type="string"),
     *                  @OA\Property(property="image", type="string"),
     *                  @OA\Property(property="is_active", type="integer"),
     *                  @OA\Property(property="category_id", type="integer"),
     *                  @OA\Property(property="author_id", type="integer"),
     *                  @OA\Property(property="added_by", type="integer"),
     *                  @OA\Property(property="updated_by", type="integer"),
     *                  @OA\Property(property="category", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="author", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                          @OA\Property(property="bio", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="added_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="updated_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *              ),
     *          )
     *       ),
     *     @OA\Header(
     *        header="Authorization",
     *        description="Authorization header with Bearer token",
     *        required=true,
     *        @OA\Schema(
     *            type="string",
     *        )
     *    )
     * )
     * @param CreateBookRequest $request
     * @return JsonResponse
     */
    public function store(CreateBookRequest $request): JsonResponse
    {
        $books = $this->bookRepository->store($request->all());
        return $this->sendApiSuccessResponse($books, 'Book Stored successfully.');
    }

    /**
     * * @OA\Put(
     * path="/api/book/update/{book}",
     * operationId="bookUpdate",
     * tags={"Book"},
     * summary="Book Update",
     * description="Book Update",
     *      @OA\Parameter(
     *         name="book",
     *         in="path",
     *         description="ID of the Book",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *          required=true,
     *          description="Book data to update",
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *               type="object",
     *               required={"title", "description", "category_id","author_id"},
     *               @OA\Property(property="title", type="string"),
     *               @OA\Property(property="description", type="string"),
     *               @OA\Property(property="book_no", type="string", nullable=true),
     *               @OA\Property(property="publication_date", type="string",format="date", nullable=true),
     *               @OA\Property(property="image", type="string" ,format="binary", nullable=true),
     *               @OA\Property(property="is_active", type="boolean"),
     *               @OA\Property(property="category_id", type="integer"),
     *               @OA\Property(property="author_id", type="integer"),
     *            ),
     *        ),
     *    ),
     *     @OA\Parameter(
     *         name="_method",
     *         in="query",
     *         required=true,
     *         description="Method override for PUT request",
     *         @OA\Schema(type="string", enum={"PUT"})
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Books Updated successfully.",
     *          @OA\JsonContent(
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="description", type="string"),
     *                  @OA\Property(property="book_no", type="string"),
     *                  @OA\Property(property="publication_date", type="string"),
     *                  @OA\Property(property="image", type="string"),
     *                  @OA\Property(property="is_active", type="integer"),
     *                  @OA\Property(property="category_id", type="integer"),
     *                  @OA\Property(property="author_id", type="integer"),
     *                  @OA\Property(property="added_by", type="integer"),
     *                  @OA\Property(property="updated_by", type="integer"),
     *                  @OA\Property(property="category", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="author", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                          @OA\Property(property="bio", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="added_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="updated_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *              ),
     *          )
     *       ),
     *     @OA\Response(
     *         response=404,
     *         description="Book not found",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="message", type="string", example="Book not found"),
     *         )
     *     ),
     *     @OA\Header(
     *        header="Authorization",
     *        description="Authorization header with Bearer token",
     *        required=true,
     *        @OA\Schema(
     *            type="string",
     *        )
     *    )
     * )
     * Update Book
     * @param Book $book
     * @param UpdateBookRequest $request
     * @return JsonResponse
     */
    public function update(Book $book, UpdateBookRequest $request): JsonResponse
    {
        $books = $this->bookRepository->update($book->id, $request->all());
        return $this->sendApiSuccessResponse($books, 'Book Updated successfully.');
    }

    /**
     * @OA\delete(
     * path="/api/book/delete/{book}",
     * security={{"bearerAuth": {}}},
     * operationId="bookDelete",
     * tags={"Book"},
     * summary="Book delete",
     * description="Book delete",
     *      @OA\Parameter(
     *         name="book",
     *         in="path",
     *         description="ID of the Book",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Book deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *     @OA\Header(
     *        header="Authorization",
     *        description="Authorization header with Bearer token",
     *        required=true,
     *        @OA\Schema(
     *            type="string",
     *        )
     *    )
     * )
     * Delete Book
     * @param Book $book
     * @return JsonResponse
     */
    public function delete(Book $book): JsonResponse
    {
        $books = $this->bookRepository->delete($book->id);
        return $this->sendApiSuccessResponse([], 'Book Deleted successfully.');
    }

    /**
     * @OA\Get(
     * path="/api/book/detail/{book}",
     * security={{"bearerAuth": {}}},
     * operationId="bookDetail",
     * tags={"Book"},
     * summary="Book detail",
     * description="Book detail",
     *      @OA\Parameter(
     *         name="book",
     *         in="path",
     *         description="ID of the Book",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Book detail fetched successfully.",
     *          @OA\JsonContent(
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="description", type="string"),
     *                  @OA\Property(property="book_no", type="string"),
     *                  @OA\Property(property="publication_date", type="string"),
     *                  @OA\Property(property="image", type="string"),
     *                  @OA\Property(property="is_active", type="integer"),
     *                  @OA\Property(property="category_id", type="integer"),
     *                  @OA\Property(property="author_id", type="integer"),
     *                  @OA\Property(property="added_by", type="integer"),
     *                  @OA\Property(property="updated_by", type="integer"),
     *                  @OA\Property(property="category", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="author", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                          @OA\Property(property="bio", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="added_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="updated_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *              ),
     *          )
     *       ),
     *     @OA\Header(
     *        header="Authorization",
     *        description="Authorization header with Bearer token",
     *        required=true,
     *        @OA\Schema(
     *            type="string",
     *        )
     *    )
     * )
     * Detail
     * @param Book $book
     * @return JsonResponse
     */
    public function bookDetail(Book $book): JsonResponse
    {
        $detail = $this->bookRepository->bookDetail($book->id);
        return $this->sendApiSuccessResponse($detail, 'Book Detail List successfully.');
    }

    /**
     * * @OA\Get(
     * path="/api/book/detail-by-category",
     * security={{"bearerAuth": {}}},
     * operationId="bookDetailByCategory",
     * tags={"Book"},
     * summary="Book detail by category",
     * description="Book detail by category",
     *     @OA\Parameter(
     *         name="category",
     *         in="query",
     *         description="Category of the Book for filter",
     *         required=false,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Book detail by category fetched successfully.",
     *          @OA\JsonContent(
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="description", type="string"),
     *                  @OA\Property(property="book_no", type="string"),
     *                  @OA\Property(property="publication_date", type="string"),
     *                  @OA\Property(property="image", type="string"),
     *                  @OA\Property(property="is_active", type="integer"),
     *                  @OA\Property(property="category_id", type="integer"),
     *                  @OA\Property(property="author_id", type="integer"),
     *                  @OA\Property(property="added_by", type="integer"),
     *                  @OA\Property(property="updated_by", type="integer"),
     *                  @OA\Property(property="category", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="author", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="slug", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                          @OA\Property(property="bio", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="added_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *                  @OA\Property(property="updated_by", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string"),
     *                          @OA\Property(property="email", type="string"),
     *                      )
     *                  ),
     *              ),
     *          )
     *       ),
     *     @OA\Header(
     *        header="Authorization",
     *        description="Authorization header with Bearer token",
     *        required=true,
     *        @OA\Schema(
     *            type="string",
     *        )
     *    )
     * )
     * Book Detail by category
     * @param Request $request
     * @return JsonResponse
     */
    public function bookDetailByCategory(Request $request): JsonResponse
    {
        $detail = $this->bookRepository->bookDetailByCategory($request->all());
        return $this->sendApiSuccessResponse($detail, 'Book Detail List successfully.');
    }
}
