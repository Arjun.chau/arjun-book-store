<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Book list with category and author
     * @param Request $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function home(Request $request)
    {
        $query = Book::with('category', 'author')->where('is_active', "1");
        if ($request->has('category'))
        {
            $query->where('category_id', $request->category);
        }
        if ($request->has('author'))
        {
            $query->where('author_id', $request->author);
        }
        $books = $query->get();
        $categories = Category::all();
        $authors = Author::all();
        return view('front.book.index', compact('books', 'categories', 'authors'));
    }

    /**
     * book Detail By Slug
     * @param $slug
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function bookDetailBySlug($slug)
    {
        $book = Book::with('category', 'author')->where('is_active', "1")->where('slug', $slug)->first();
        $categories = Category::all();
        $authors = Author::all();
        return view('front.book.detail', compact('book', 'categories', 'authors'));
    }
}
