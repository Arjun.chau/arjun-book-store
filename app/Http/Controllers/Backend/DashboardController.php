<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        return view('admin.dashboard.dashboard')->with('message', 'Welcome to Dashboard');
    }
}
