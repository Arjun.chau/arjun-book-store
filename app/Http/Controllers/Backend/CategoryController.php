<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Validator;

class CategoryController extends Controller
{
    /**
     * @return mixed
     */
    public function index(): mixed
    {
        $categories = Category::all();
        return view('admin.category.index', compact('categories'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $data = [
            'name' => $request->name,
        ];
        Category::create($data);
        return redirect('category/index')->with('message', 'Category created successfully');
    }

    /**
     * @param Category $category
     * @return mixed
     */
    public function edit(Category $category)
    {
        $categories = Category::where('id', $category->id)->first();
        return view('admin.category.edit', compact('categories'));
    }

    /**
     * @param Category $category
     * @return mixed
     */
    public function show(Category $category)
    {
        $categories = Category::where('id', $category->id)->first();
        return view('admin.category.show', compact('categories'));
    }

    /**
     * @param Category $category
     * @param Request $request
     * @return mixed
     */
    public function update(Category $category, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $data = [
            'name' => $request->name,
        ];

        Category::where('id', $category->id)->update($data);
        return redirect('category/index')->with('info', 'Category updated successfully');
    }

    /**
     * @param Category $category
     * @return mixed
     */
    public function delete(Category $category)
    {
        $categoryData = Category::where('id', $category->id)->first();
        $categoryData->delete();
        return redirect('category/index')->with('warning', 'Category deleted successfully');
    }
}
