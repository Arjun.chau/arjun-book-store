<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use App\Repositories\Interfaces\BookInterface;
use Illuminate\Http\Request;

class BookController extends Controller
{
    private $bookRepository;

    public function __construct(BookInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }
    /**
     * @return mixed
     */
    public function index(): mixed
    {
        $books = $this->bookRepository->index();
        return view('admin.book.index', compact('books'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $categoryLists = Category::get();
        $authorLists = Author::get();
        return view('admin.book.create', compact('categoryLists', 'authorLists'));
    }

    /**
     * @param CreateBookRequest $request
     * @return mixed
     */
    public function store(CreateBookRequest $request)
    {
        $requestLists = $this->bookRepository->store($request->all());
        return redirect('book/index')->with('message', 'Book created successfully');
    }

    /**
     * @param Book $book
     * @return mixed
     */
    public function edit(Book $book)
    {
        $books = Book::where('id',$book->id)->with('category', 'author', 'user_added', 'user_updated')->first();
        $categoryLists = Category::get();
        $authorLists = Author::get();
        return view('admin.book.edit', compact('books', 'categoryLists', 'authorLists'));
    }

    /**
     * @param Book $book
     * @param UpdateBookRequest $request
     * @return mixed
     */
    public function update(Book $book, UpdateBookRequest $request)
    {
        $requestLists = $this->bookRepository->update($book->id, $request->all());
        return redirect('book/index')->with('info', 'Book updated successfully');
    }

    /**
     * @param Book $book
     * @return mixed
     */
    public function delete(Book $book)
    {
        $this->bookRepository->delete($book->id);
        return redirect('book/index')->with('warning', 'Book deleted successfully');
    }
}
