<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Validator;

class AuthorController extends Controller
{
    /**
     * @return mixed
     */
    public function index(): mixed
    {
        $authors = Author::all();
        return view('admin.author.index', compact('authors'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('admin.author.create');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $validateRequest = $request->all();
        $validationResult  = $this->validationData($validateRequest);
        if ($validationResult['errors']) {
            // If there are validation errors, redirect back with errors
            return redirect()->back()->withErrors($validationResult['errors']);
        }
        Author::create($validationResult['data']);
        return redirect('author/index')->with('message', 'Author created successfully');
    }

    /**
     * @param Author $author
     * @return mixed
     */
    public function edit(Author $author)
    {
        $authors = Author::where('id', $author->id)->first();
        return view('admin.author.edit', compact('authors'));
    }

    /**
     * @param Author $author
     * @return mixed
     */
    public function show(Author $author)
    {
        $authors = Author::where('id', $author->id)->first();
        return view('admin.author.show', compact('authors'));
    }

    /**
     * @param Author $author
     * @param Request $request
     * @return mixed
     */
    public function update(Author $author, Request $request)
    {
        $validateRequest = $request->all();
        $validationResult  = $this->validationData($validateRequest);
        if ($validationResult['errors']) {
            // If there are validation errors, redirect back with errors
            return redirect()->back()->withErrors($validationResult['errors']);
        }
        Author::where('id', $author->id)->update($validationResult['data']);
        return redirect('author/index')->with('info', 'Author updated successfully');
    }

    /**
     * @param Author $author
     * @return mixed
     */
    public function delete(Author $author)
    {
        $authorData = Author::where('id', $author->id)->first();
        $authorData->delete();
        return redirect('author/index')->with('warning', 'Author deleted successfully');
    }

    /**
     * @param $validateRequest
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function validationData($validateRequest)
    {
        $validator = Validator::make($validateRequest, [
            'name' => 'required',
            'email' => 'required|email',
            'bio' => 'nullable',
        ]);

        if ($validator->fails()) {
            return [
                'errors' => $validator->errors(),
                'data' => null,
            ];
        }

        $data = [
            'name' => $validateRequest['name'],
            'email' => $validateRequest['email'],
            'bio' => $validateRequest['bio'],
        ];
        return [
            'errors' => null,
            'data' => $data,
        ];
    }
}
