<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Book extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'slug',
        'description',
        'book_no',
        'publication_date',
        'image',
        'is_active',
        'category_id',
        'author_id',
        'added_by',
        'updated_by'
    ];

    /**
     * Boot the model.
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function ($book) {
            $book->slug = $book->createSlug($book->title);
            $book->save();
        });
    }

    /**
     * Write code on Method
     *
     * @return mixed
     */
    private function createSlug($title){
        if (static::whereSlug($slug = Str::slug($title))->exists()) {
            $max = static::whereTitle($title)->latest('id')->skip(1)->value('slug');

            if (is_numeric($max[-1])) {
                return preg_replace_callback('/(\d+)$/', function ($mathces) {
                    return $mathces[1] + 1;
                }, $max);
            }

            return "{$slug}-2";
        }

        return $slug;
    }

    /**
     * One to One Relationship with Category
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * One to One Relationship with Author
     */
    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    /**
     * One to One Relationship with user
     */
    public function user_added()
    {
        return $this->belongsTo(User::class, 'added_by');
    }

    /**
     * One to One Relationship with user
     */
    public function user_updated()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function getExcerptAttribute()
    {
        return substr($this->description,0, 50);
    }

    public function getExcerptFullAttribute()
    {
        return substr($this->description,0, 600);
    }
}
