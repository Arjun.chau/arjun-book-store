<?php


namespace App\Repositories\Interfaces;


interface BookInterface
{
    /**
     * Index
     * @return mixed
     */
    public function index(): mixed;

    /**
     * Create
     * @param $bookDetails
     * @return mixed
     */
    public function store($bookDetails): mixed;

    /**
     * Update
     * @param int $bookId
     * @param $bookDetails
     * @return mixed
     */
    public function update(int $bookId, $bookDetails): mixed;

    /**
     * Delete
     * @param int $bookId
     * @return mixed
     */
    public function delete(int $bookId): mixed;

    /**
     * Detail
     * @param int $bookId
     * @return mixed
     */
    public function bookDetail(int $bookId): mixed;

    /**
     * Detail
     * @param $filter
     * @return mixed
     */
    public function bookDetailByCategory($filter): mixed;
}
