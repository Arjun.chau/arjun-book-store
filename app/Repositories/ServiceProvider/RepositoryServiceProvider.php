<?php

namespace App\Repositories\ServiceProvider;

use App\Http\Controllers\Api\ApiBookController;
use App\Http\Controllers\Backend\BookController;
use App\Repositories\Interfaces\BookInterface;
use App\Repositories\Services\BookRepository;
use Illuminate\Support\ServiceProvider;


class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(BookController::class)
            ->needs(BookInterface::class)
            ->give(BookRepository::class);
        $this->app->when(ApiBookController::class)
            ->needs(BookInterface::class)
            ->give(BookRepository::class);
    }
}
