<?php

namespace App\Repositories\Services;

use App\Models\Book;
use App\Repositories\Interfaces\BookInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookRepository implements BookInterface
{
    /**
     * Index Book
     * @return mixed
     */
    public function index(): mixed
    {
        return Book::with('category', 'author', 'user_added', 'user_updated')->get();
    }

    /**
     * Create New Book
     * @param $bookDetails
     * @return mixed
     */
    public function store($bookDetails): mixed
    {
        $imageName = time() . '.' . $bookDetails['image']->extension();
        $bookDetails['image']->move(public_path('books'), $imageName);
        $books = [
            'title' => $bookDetails['title'],
            'description' => $bookDetails['description'],
            'book_no' => $bookDetails['book_no'],
            'publication_date' => $bookDetails['publication_date'],
            'category_id' => $bookDetails['category_id'],
            'author_id' => $bookDetails['author_id'],
            'image' => $imageName,
            'is_active' => array_key_exists('is_active', $bookDetails) ? "1" : "0",
            'added_by' => auth()->check() ? Auth::user()->id : null,
        ];
        Book::create($books);
        return Book::latest()->first();
    }

    /**
     * Update Book
     * @param int $bookId
     * @param $bookDetails
     * @return mixed
     */
    public function update(int $bookId, $bookDetails): mixed
    {
        $list = Book::where('id', $bookId)->first();
        if (array_key_exists('image', $bookDetails)) {
            @unlink(public_path('/books/' . $list->image));
            $imageName = time() . '.' . $bookDetails['image']->extension();

            $bookDetails['image']->move(public_path('books'), $imageName);
            $books = [
                'title' => $bookDetails['title'],
                'description' => $bookDetails['description'],
                'book_no' => $bookDetails['book_no'],
                'publication_date' => $bookDetails['publication_date'],
                'category_id' => $bookDetails['category_id'],
                'author_id' => $bookDetails['author_id'],
                'image' => $imageName,
                'is_active' => array_key_exists('is_active', $bookDetails) ? "1" : "0",
                'updated_by' => auth()->check() ? Auth::user()->id : null,
            ];
        } else {
            $books = [
                'title' => $bookDetails['title'],
                'description' => $bookDetails['description'],
                'book_no' => $bookDetails['book_no'],
                'publication_date' => $bookDetails['publication_date'],
                'category_id' => $bookDetails['category_id'],
                'author_id' => $bookDetails['author_id'],
                'is_active' => array_key_exists('is_active', $bookDetails) ? "1" : "0",
                'updated_by' => auth()->check() ? Auth::user()->id : null,
            ];
        }
        Book::where('id', $bookId)->update($books);
        return Book::where('id', $bookId)->first();
    }

    /**
     * Delete Book
     *
     * @param int $bookId
     * @return mixed
     */
    public function delete(int $bookId): mixed
    {
        $book = Book::find($bookId);
        @unlink(public_path('/books/' . $book->image));
        $book->delete();
        return true;
    }

    /**
     * Detail of Book by category
     * @param int $bookId
     * @return mixed
     */
    public function bookDetail(int $bookId): mixed
    {
        return Book::where('id', $bookId)->with('category', 'author', 'user_added', 'user_updated')->first();
    }

    /**
     * Detail of Book by category
     * @param $filter
     * @return mixed
     */
    public function bookDetailByCategory($filter): mixed
    {
        $query = Book::with('category', 'author', 'user_added', 'user_updated')->where('is_active' , "1");
        if ($filter['category'])
        {
            $query->where('category_id', $filter['category']);
        }
        return $query->get();
    }
}
