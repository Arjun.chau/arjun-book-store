@extends('admin.layouts.app')
@section('content')
    <div class="pagetitle">
        <h1>Books</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('book.index')}}">Home</a></li>
                <li class="breadcrumb-item">Books</li>
                <li class="breadcrumb-item active">Index</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <a href="{{route('book.create')}}"><button type="button" class="btn btn-success mt-3">Add Book</button></a>
                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Description</th>
                                <th scope="col">publication Date</th>
                                <th scope="col">Image</th>
                                <th scope="col">Status</th>
                                <th scope="col">Category Name</th>
                                <th scope="col">Author Name</th>
                                <th scope="col">Added By</th>
                                <th scope="col">Updated By</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($books as $book)
                                <tr>
                                    <th scope="row">{{ $loop->index + 1 }}</th>
                                    <td>{{$book->title}}</td>
                                    <td>{{strip_tags($book->description)}}</td>
                                    <td>{{$book->publication_date}}</td>
                                    <td>
                                        <img src="{{ asset('/books/'.$book->image) }}" alt="Image" height="100" width="100">
                                    </td>
                                    <td>
                                        <input class="form-check-input" type="checkbox" name="is_active" value="1" {{ $book->is_active ? 'checked' : '' }} disabled>
                                    </td>
                                    <td>{{$book->category->name}}</td>
                                    <td>{{$book->author->name}}</td>
                                    <td>{{isset($book->user_added) ? $book->user_added->name : "Null"}}</td>
                                    <td>{{isset($book->user_updated) ? $book->user_updated->name : "Null"}}</td>
                                    <td class="d-block">
                                        <a href="{{route('book.edit', ['book' => $book->id])}}"><i class="bi bi-pencil-square"></i></a>
{{--                                        <a href="{{route('book.show', ['book' => $book->id])}}"><i class="bi bi-folder2-open"></i></a>--}}
                                        <a href="{{route('book.delete', ['book' => $book->id])}}"><i class="bi bi-trash-fill"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
