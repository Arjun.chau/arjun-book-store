@extends('admin.layouts.app')
@section('content')
    <div class="pagetitle">
        <h1>Books</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('book.index')}}">Home</a></li>
                <li class="breadcrumb-item">Books</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"></h5>

                        <form action="{{route('book.update',['book' => $books->id])}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Book Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="title" value="{{ $books->title ?? '' }}">
                                    @if($errors->has('title'))
                                        <div style="color: red">{{ $errors->first('title') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputEmail" class="col-sm-2 col-form-label">Description</label>
                                <!-- TinyMCE Editor -->
                                <div class="col-sm-10">
                                    <textarea class="form-control tinymce-editor" id="basic-example" name="description">
                                    </textarea>
                                    @if($errors->has('description'))
                                        <div style="color: red">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                                <!-- End TinyMCE Editor -->
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Book Number</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="book_no" value="{{$books->book_no}}">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Publication Date</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" name="publication_date" value="{{$books->publication_date}}">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="image">
                                    <img src="{{ asset('/books/'.$books->image) }}" alt="Old Image" height="100" width="100">
                                    <p>File type: png, jpeg, jpg</p>
                                    @if($errors->has('image'))
                                        <div style="color: red">{{ $errors->first('image') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Is Active</label>
                                <div class="col-sm-10 form-check form-switch">
                                    <input class="form-check-input" style="margin-left: -3px;" type="checkbox" id="isActive" name="is_active" {{ $books->is_active ? 'checked' : '' }}>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Category</label>
                                {{--                                    <label class="title-warning">* </label>--}}
                                <div class="col-sm-10">
                                    <select name="category_id" id="category_id">
                                        <option value="">Please Select Category</option>
                                        @foreach($categoryLists as $category)
                                            <option value="{{ $category->id }}" {{ $category->id == $books->category_id ? 'selected' : '' }}>{{ $category->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('category_id'))
                                        <div style="color: red">{{ $errors->first('category_id') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Author</label>
                                {{--                                    <label class="title-warning">* </label>--}}
                                <div class="col-sm-10">
                                    <select name="author_id" id="author_id">
                                        <option value="">Please Select Author</option>
                                        @foreach($authorLists as $author)
                                            <option value="{{ $author->id }}" {{ $author->id == $books->author_id ? 'selected' : '' }}>{{ $author->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('author_id'))
                                        <div style="color: red">{{ $errors->first('author_id') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary text-right">Update</button>
                                </div>
                            </div>

                        </form><!-- End General Form Elements -->

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script>
        let description = {!! json_encode($books) ?? '' !!};
        tinymce.init({
            selector: 'textarea#basic-example',
            height: 500,
            plugins: [
                'advlist', 'autolink', 'lists','charmap', 'preview',
                'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
                'insertdatetime', 'wordcount'
            ],
            toolbar: 'undo redo | blocks | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }',
            promotion: false,
            setup: function (editor) {
                editor.on('init', function (e) {
                    editor.setContent(description.description);
                });
            }
        });
    </script>
@endpush
