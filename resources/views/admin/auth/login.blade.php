<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Login Page</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    @include('admin.layouts.styles')

</head>

<body>

<main>
    <div class="container">

        <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

                        <div class="card mb-3">

                            <div class="card-body">

                                <div class="pt-4 pb-2">
                                    <h5 class="card-title text-center pb-0 fs-4">Login to Book Store</h5>
                                    <p class="text-center small">Enter your email & password to login</p>
                                </div>
                                @if (Session::get('success'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        {{ Session::get('success') }}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                @endif
                                <form class="row g-3 needs-validation" action="{{route('login')}}" method="POST">
                                    @csrf
                                    @if ($errors->has('message'))
                                        <div class=" col-12 alert alert-danger alert-dismissible fade show" role="alert">
                                            {{ $errors->first('message') }}
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    </span>
                                    @endif
                                    <div class="col-12">
                                        <label for="yourUsername" class="form-label">Email</label>
                                        <input type="email" name="email" class="form-control" id="email"
                                               required>
                                        <div class="invalid-feedback">Please enter your username.</div>
                                    </div>

                                    <div class="col-12">
                                        <label for="yourPassword" class="form-label">Password</label>
                                        <input type="password" name="password" class="form-control" id="password"
                                               required>
                                        <div class="invalid-feedback">Please enter your password!</div>
                                    </div>
                                    <div class="col-12">
                                        <button class="btn btn-primary w-100" type="submit">Login</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

    </section>

    </div>
</main><!-- End #main -->

@include('admin.layouts.scripts')
@include('notification.notification')
</body>

</html>
