@extends('admin.layouts.app')
@section('content')
    <div class="pagetitle">
        <h1>Authors</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('author.index')}}">Home</a></li>
                <li class="breadcrumb-item">Authors</li>
                <li class="breadcrumb-item active">Index</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <a href="{{route('author.create')}}"><button type="button" class="btn btn-success mt-3">Add Author</button></a>
                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Author Name</th>
                                <th scope="col">Author Email</th>
                                <th scope="col">Bio</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($authors as $author)
                                <tr>
                                    <th scope="row">{{ $loop->index + 1 }}</th>
                                    <td>{{$author->name}}</td>
                                    <td>{{$author->email}}</td>
                                    <td>{{$author->bio}}</td>
                                    <td class="d-block">
                                        <a href="{{route('author.edit', ['author' => $author->id])}}"><i class="bi bi-pencil-square"></i></a>
                                        <a href="{{route('author.show', ['author' => $author->id])}}"><i class="bi bi-folder2-open"></i></a>
                                        <a href="{{route('author.delete', ['author' => $author->id])}}"><i class="bi bi-trash-fill"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
