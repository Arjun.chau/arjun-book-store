@extends('admin.layouts.app')
@section('content')
    <div class="pagetitle">
        <h1>Author</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('author.index')}}">Home</a></li>
                <li class="breadcrumb-item">Author</li>
                <li class="breadcrumb-item active">Show</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Author Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name" value="{{ $authors->name ?? '' }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Author Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="email" value="{{ $authors->email ?? '' }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Author Bio</label>
                                <div class="col-sm-10">
                                    <textarea type="text" class="form-control" name="bio" disabled>{{ $authors->bio ?? '' }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <a href="{{route('author.index')}}"><button type="" class="btn btn-primary text-right">Back</button></a>
                                </div>
                            </div>

                        <!-- End General Form Elements -->
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
@push('scripts')
@endpush
