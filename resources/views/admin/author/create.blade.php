@extends('admin.layouts.app')
@section('content')
    <div class="pagetitle">
        <h1>Authors</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('author.index')}}">Home</a></li>
                <li class="breadcrumb-item">Authors</li>
                <li class="breadcrumb-item active">Create</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"></h5>

                        <form action="{{route('author.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Author Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name">
                                    @if($errors->has('name'))
                                        <div style="color: red">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Author Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="email">
                                    @if($errors->has('email'))
                                        <div style="color: red">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Author Bio</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="bio"></textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary text-right">Submit</button>
                                </div>
                            </div>
                        </form><!-- End General Form Elements -->

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
@push('scripts')
@endpush
