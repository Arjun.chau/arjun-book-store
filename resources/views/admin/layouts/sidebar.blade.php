<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link " href="{{route('dashboard')}}">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li><!-- End Dashboard Nav -->

        <li class="nav-item">
            <a class="nav-link collapsed" href="{{route('category.index')}}">
                <i class="bi bi-bookmark"></i>
                <span>Category</span>
            </a>
        </li>
        <!-- End Category Page Nav -->

        <li class="nav-item">
            <a class="nav-link collapsed" href="{{route('author.index')}}">
                <i class="bi bi-person"></i>
                <span>Author</span>
            </a>
        </li>
        <!-- End Author Page Nav -->

        <li class="nav-item">
            <a class="nav-link collapsed" href="{{route('book.index')}}">
                <i class="bi bi-book"></i>
                <span>Book</span>
            </a>
        </li>
        <!-- End Book Page Nav -->
    </ul>

</aside>
