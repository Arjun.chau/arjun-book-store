<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Dashboard - NiceAdmin Bootstrap Template</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Style include -->
    @include('admin.layouts.styles')
</head>

<body class="d-flex flex-column min-vh-100">

<!-- ======= Header ======= -->
@include('admin.layouts.header')
<!-- End Header -->

<!-- ======= Sidebar ======= -->
@include('admin.layouts.sidebar')
<!-- End Sidebar-->

<main id="main" class="main">

    @yield('content')
{{--    <div class="pagetitle">--}}
{{--        <h1>Dashboard</h1>--}}
{{--        <nav>--}}
{{--            <ol class="breadcrumb">--}}
{{--                <li class="breadcrumb-item"><a href="">Home</a></li>--}}
{{--                <li class="breadcrumb-item active">Dashboard</li>--}}
{{--            </ol>--}}
{{--        </nav>--}}
{{--    </div>--}}
    <!-- End Page Title -->

</main>
<!-- End #main -->

<!-- ======= Footer ======= -->
@include('admin.layouts.footer')
<!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
@include('admin.layouts.scripts')
@stack('scripts');
<script src="{{asset('assets/js/main.js')}}"></script>
@include('notification.notification')
</body>

</html>
